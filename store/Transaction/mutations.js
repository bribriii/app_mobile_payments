export default {
    setInvoiceList(state, invoiceData) {
        state.invoicesList = invoiceData;
    },
    setSelectedInvoices(state, invoiceData) {
        state.selectedInvoices = invoiceData;
    },
    setAcknowledgementSlips(state, acknowledgmentSlips){
        state.acknowledgmentSlips = acknowledgmentSlips;
    }
    
    // setTransferModalData(state,data){
    //     state.transferModalData = data
    // },

    // setTransmitMainTableData(state, data){
    //     state.transmitMainTableData = data
    // },

    // setVoidData(state,data){
    //     state.voidData = data
    // },
    // setUserDetails(state, data){
    //     state.userDetails = data;
    // },

    // addCreatedTransaction(state, transactionData) {
    //     state.transferModalData.push(transactionData)
    // },

    
}