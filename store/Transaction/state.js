export default {
    invoicesList:[],
    selectedInvoices: [{
        invoiceNum: null, 
        amount: null, 
        paymentSlipDetails: { 
            date: null, 
            supplier: null, 
            item: null, 
            weightSlipNo: null, 
            quantity: null, 
            netWeight: null }, 
        supplierCode: null
    }],
    acknowledgmentSlips: []
    
}