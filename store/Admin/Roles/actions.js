import axios from "axios";
export default {
  async fetchListRoles({ commit }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/roles/get-all`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      }
    })
      .then(res => {
        if (Array.isArray(res.data.fetched))
          commit("setRoleList", res.data.fetched);
        else commit("setRoleList", []);

        return res;
      })
      .catch(err => err);
  },

  async addRole(
    { commit },
    { roleForm, SessionId }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/roles/add`,
      data: {
        roleInfo: {...roleForm},
      },
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
    })
      .then(res => {
        commit("addRole", res.data.posted.posted);
        return res
      })
      .catch(err => err);
  },

  async editRole(
    { commit },
    { roleForm, SessionId }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/roles/update`,
      data: {
        roleInfo: {...roleForm},
      },
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
    })
      .then(res => {
        // commit("editRole", res.data.posted.posted);
        return res
      })
      .catch(err => err);
  }
}
