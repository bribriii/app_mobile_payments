let id = 1;
export default {
  addModule(state, moduleData) {
    state.modulesList.unshift(moduleData);
  },
  editModule(state, moduleData) {
    const index = state.modulesList.findIndex(
      modules => modules.id == moduleData.id
    );

    state.modulesList.splice(index, 1);
    state.modulesList.unshift(moduleData);
  },
  setModuleList(state, moduleData) {
    state.modulesList = moduleData;
  }
};
