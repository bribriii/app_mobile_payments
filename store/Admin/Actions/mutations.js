export default {
  setActionsList(state, data) {
    state.actionsList = data;
  },
  addAction(state, actionsData) {
    state.actionsList.unshift(actionsData);
  },
  editAction(state, actionsData) {
    const index = state.actionsList.findIndex(
      actions => actions.id == actionsData.id
    );

    state.actionsList.splice(index, 1);
    state.actionsList.unshift(actionsData);
  },
};
