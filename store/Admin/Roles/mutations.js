export default {
  addRole(state, data) {
    state.rolesList.push(data);
  },

  editRole(state, roleForm) {
    const index = state.rolesList.findIndex(
      roles => roles.id == roleForm.id
    );

    state.rolesList.splice(index, 1);
    state.rolesList.unshift(roleForm);
  },

  setRoleList(state, data) {
    state.rolesList = data;
  }
};
