let id = 1;

export default {
  setUserList(state, usersData) {
    state.usersList = usersData;
  },
  addUser(state, data) {
    state.usersList.push(data);
  },
  editUser(state, usersData) {
    const index = state.usersList.findIndex(
      users => users.id == usersData.id
    );

    state.usersList.splice(index, 1);
    state.usersList.unshift(usersData);
  },
  setCompanyList(state, usersData) {
    state.companyList = usersData;
  },
};
