import axios from "axios";
export default {
  async fetchListUsers({ commit }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/users/all`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      }
    })
      .then(res => {
        console.log(res.data);
        if (Array.isArray(res.data.fetched))
          commit("setUserList", res.data.fetched);
        else commit("setUserList", []);

        return res;
      })
      .catch(err => err);
  },
  async addUser(
    { commit },
    { usersData, SessionId }
  ) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/users/add`,
      data: {
        ...usersData
      },
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
    })
      .then(res => {
        commit("addUser", res.data.posted);
        return res
      })
      .catch(err => err);
  },

  async editUser(
    { commit },
    { usersData, SessionId }
  ) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/users/update/${usersData.id}`,
      data: {
        ...usersData
      },
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
    })
      .then(res => {
        // commit("editRole", res.data.posted.posted);
        return res
      })
      .catch(err => err);
  },
  async fetchListCompany({ commit }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/company`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      }
    })
      .then(res => {
        console.log(res.data);
        if (Array.isArray(res.data.fetched))
          commit("setCompanyList", res.data.fetched);
        else commit("setCompanyList", []);

        return res;
      })
      .catch(err => err);
  },

}