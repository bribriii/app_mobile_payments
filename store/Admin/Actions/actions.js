import axios from "axios";
export default {
  async fetchListActions({ commit }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/actions/get-all`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      }
    })
      .then(res => {
        if (Array.isArray(res.data.fetched))
          commit("setActionsList", res.data.fetched);
        else commit("setActionsList", []);

        return res;
      })
      .catch(err => err);
  },

  async addAction({ commit }, { actionsData, SessionId }) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/actions/add`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
      data: {
        ...actionsData
      }
    })
      .then(res => {
        console.log(res.data);
        commit("addAction", res.data.posted)

        return res;
      })
      .catch(err => err);
  },

  async editAction({ commit }, { actionsData, SessionId }) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/actions/update/${actionsData.id}`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
      data: {
        ...actionsData
      }
    })
      .then(res => {
        // commit("editAction", res.data.put.data[0])

        return res;
      })
      .catch(err => err);
  }
}
