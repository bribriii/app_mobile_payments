import axios from "axios";
export default {
  async fetchListInvoice({ commit }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/transactions/open-invoices`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      }
    })
      .then(res => {
        if (Array.isArray(res.data.fetched))
          commit("setInvoiceList", res.data.fetched);
        else commit("setInvoiceList", []);

        return res;
      })
      .catch(err => err);
  },
  async fetchAcknowledgmentSlips({ commit }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/transactions/acknowledgement-slips`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      }
    })
      .then(res => {
        if (Array.isArray(res.data.fetched)){
          for (let i = 0; i < res.data.fetched.length; i++) {
            const item = res.data.fetched[i];
            
            item.claimDetails.imagePath = `${this.$axios.defaults.baseURL}/` + item.claimDetails.imagePath;
            item.claimDetails.spaDocImagepath = `${this.$axios.defaults.baseURL}/` + item.claimDetails.spaDocImagepath;
            item.claimDetails.idImagePath = `${this.$axios.defaults.baseURL}/` + item.claimDetails.idImagePath;

          }
          console.log(res.data);
          commit("setAcknowledgementSlips", res.data.fetched);
        }
        else commit("setAcknowledgementSlips", []);

        return res;
      })
      .catch(err => err);
  },
  async fetchSelectedInvoices({ commit }, { docNumArr }) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/transactions/open-invoices`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
      data: {
        docNumArr
      }
    })
      .then(res => {
        console.log(res.data)
        if (Array.isArray(res.data.fetched))
          commit("setSelectedInvoices", res.data.fetched);
        else commit("setSelectedInvoices", []);

        return res;
      })
      .catch(err => err);
  },
  async verifyPin({ commit }, { OTP, supplierCode }) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/transactions/verify-pin`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
      data: {
        OTP, supplierCode
      }
    })
      .then(res => {
        return res.data.fetched
      })
      .catch(err => {
        return err.response
      })
  },
  async processPaymentSlips({ commit },  {fd} ) {
    console.log(fd)
    return await axios
        .put(`${this.$axios.defaults.baseURL}/transactions/payment-transaction`, fd, {
          headers: {
            SessionId: `${localStorage.SessionId}`
          },
        })
        .then(res => {
          return res.data.path;
        })
        .catch(err => {
          console.log(err);
        });
  },


}