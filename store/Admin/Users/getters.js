export default {
  getUsersList(state) {
    return state.usersList;
  },
  getCompanyList(state) {
    return state.companyList;
  }
};
