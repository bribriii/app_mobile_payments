import axios from "axios";
export default {
  async fetchListModules({ commit }) {
    return await axios({
      method: "GET",
      url: `${this.$axios.defaults.baseURL}/modules/get-all`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      }
    })
      .then(res => {
        console.log(res.data);
        if (Array.isArray(res.data.fetched))
          commit("setModuleList", res.data.fetched);
        else commit("setModuleList", []);

        return res;
      })
      .catch(err => err);
  },

  async addModule({ commit }, { moduleData, SessionId }) {
    return await axios({
      method: "POST",
      url: `${this.$axios.defaults.baseURL}/modules/add`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
      data: {
        ...moduleData
      }
    })
      .then(res => {
        commit("addModule", res.data.posted.posted)
        return res;
      })
      .catch(err => err);
  },

  async editModule({ commit }, { moduleData, SessionId }) {
    return await axios({
      method: "PUT",
      url: `${this.$axios.defaults.baseURL}/modules/update/${moduleData.id}`,
      headers: {
        SessionId: `${localStorage.SessionId}`
      },
      data: {
        ...moduleData
      }
    })
      .then(res => {
        // commit("editModule", res.data.put.data[0])

        return res;
      })
      .catch(err => err);
  },
}