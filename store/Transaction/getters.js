export default {
    getInvoiceList(state){
        return state.invoicesList
    },
    getSelectedInvoices(state){
        return state.selectedInvoices
    },
    getAcknowledgmentSlips(state){
        return state.acknowledgmentSlips
    }
}